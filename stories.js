/*
	Retrieve data from database.
*/

function loadAllStoriesTitlesAndIds() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", "https://xjunctioncustomapi.azurewebsites.net/story", false ); 
    xmlHttp.send( null );
    stories = JSON.parse(xmlHttp.responseText);
}

function loadStoryWithId(id) {
    var xmlHttp = new XMLHttpRequest();
	console.log(id)
	xmlHttp.open( "GET", "https://xjunctioncustomapi.azurewebsites.net/story?id=" + id, false );

	xmlHttp.setRequestHeader('Content-type','application/json');
	xmlHttp.setRequestHeader('Access-Control-Allow-Methods','GET');

    xmlHttp.send( null );
    return JSON.parse(xmlHttp.responseText)[0];
}
var stories;
loadAllStoriesTitlesAndIds()